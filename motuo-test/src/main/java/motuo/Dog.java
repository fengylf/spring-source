package motuo;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author: lifeng
 * @date: 2021/3/1 17:32
 * @description:
 */
@Component
public class Dog implements ApplicationContextAware{

	private ApplicationContext applicationContext;


	public void bite() {
		System.out.println("dog bite");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
